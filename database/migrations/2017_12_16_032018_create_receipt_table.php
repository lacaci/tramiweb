<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sender_id');
            $table->unsignedInteger('recipient_id');
            $table->unsignedInteger('agency_id');
            $table->unsignedInteger('trip_id');
            $table->unsignedInteger('user_id');
            $table->unsignedSmallInteger('number');
            $table->boolean('active')->default(1);

            $table->timestamps();

            $table->foreign('sender_id')->references('id')->on('sender');
            $table->foreign('recipient_id')->references('id')->on('recipient');
            $table->foreign('agency_id')->references('id')->on('agency');
            $table->foreign('trip_id')->references('id')->on('trip');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt');
    }
}
