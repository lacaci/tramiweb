<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientSenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sender', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('agency_id');
            $table->string('first_name', 45);
            $table->string('last_name', 45);
            $table->string('address', 250)->nullable();
            $table->string('city', 45)->nullable();
            $table->unsignedInteger('country_id');
            $table->string('phone_1', 20);
            $table->string('phone_2', 20)->nullable();
            $table->string('email', 40)->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();

            $table->foreign('agency_id')->references('id')->on('agency');
            $table->foreign('country_id')->references('id')->on('country');
        });

        Schema::create('recipient', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('agency_id');
            $table->string('first_name', 45);
            $table->string('middle_name', 45);
            $table->string('last_name', 45);
            $table->string('maiden_name', 45);
            $table->string('address', 250);

            $table->unsignedInteger('country_id');
            $table->unsignedInteger('province_id');
            $table->unsignedInteger('municipality_id')->nullable();
            $table->unsignedInteger('borough_id')->nullable();

            $table->string('phone_1', 20);
            $table->string('phone_2', 20)->nullable();
            $table->string('email', 40)->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();

            $table->foreign('agency_id')->references('id')->on('agency');
            $table->foreign('province_id')->references('id')->on('province');
            $table->foreign('municipality_id')->references('id')->on('municipality');
            $table->foreign('borough_id')->references('id')->on('borough');
            $table->foreign('country_id')->references('id')->on('country');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipient');
        Schema::dropIfExists('sender');
    }
}
