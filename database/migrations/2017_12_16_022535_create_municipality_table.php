<?php

use App\Models\Municipality;
use App\Models\Province;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipality', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('province_id');
            $table->string('label', 40);
            $table->timestamps();

            $table->foreign('province_id')->references('id')->on('province');
        });

        $province = new Province();
        $municipality = new Municipality();

        $province = $province->where('label', '=', 'pinar del rio')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'consolacion del sur']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guane']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'la palma']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'los palacios']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'mantua']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'minas de matahambre']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'pinar del rio']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san juan y martinez']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san luis']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'sandino']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'vinales']);

        $province = $province->where('label', '=', 'artemisa')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'alquizar']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'artemisa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'bauta']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'caimito']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guanajay']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guira de melena']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'mariel']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san antonio de los banos']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'candelaria']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san cristobal']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'bahia honda']);

        $province = $province->where('label', '=', 'ciudad de la habana')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'arroyo naranjo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'boyeros']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'centro habana']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cerro']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cotorro']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'diez de octubre']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guanabacoa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'habana del este']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'habana vieja']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'la lisa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'marianao']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'playa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'plaza de la revolucion']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'regla']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san miguel del padron']);

        $province = $province->where('label', '=', 'mayabeque')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'batabano']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'bejucal']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guines']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'jaruco']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'madruga']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'melena del sur']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'nueva paz']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'quivican']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san jose de las lajas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san nicolas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'santa cruz del norte']);

        $province = $province->where('label', '=', 'matanzas')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'calimete']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cardenas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cienaga de zapata']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'colon']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'jaguey grande']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'jovellanos']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'limonar']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'los arabos']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'marti']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'matanzas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'pedro betancourt']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'perico']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'union de reyes']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'varadero']);

        $province = $province->where('label', '=', 'cienfuegos')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'abreus']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'aguada de pasajeros']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cienduegos']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cruces']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cumanayagua']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'lajas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'palmira']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'rodas']);

        $province = $province->where('label', '=', 'villa clara')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'caibarien']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'camajuani']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cifuentes']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'corralillo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'encrucijada']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'manicaragua']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'placetas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'quemado de guines']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'ranchuelo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san juan de los remedios']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'sagua la grande']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'santa clara']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'santo domingo']);

        $province = $province->where('label', '=', 'sancti spiritus')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'cabaiguan']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'fomento']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'jatibonico']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'la sierpe']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'sancti spiritus']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'taguasco']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'trinidad']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'yaguajay']);

        $province = $province->where('label', '=', 'ciego de avila')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'baragua']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'bolivia']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'chambas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'ciego de avila']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'ciro redondo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'florencia']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'majagua']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'moron']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'primero de enero']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'venezuela']);

        $province = $province->where('label', '=', 'camaguey')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'camaguey']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'carlos manuel de cespedes']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'esmeralda']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'florida']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guaimaro']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'jimaguayu']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'minas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'najasa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'nuevitas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'santa cruz del sur']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'sibanicu']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'sierra de cubitas']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'vertientes']);

        $province = $province->where('label', '=', 'las tunas')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'amancio']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'colombia']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'jesus menendez']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'jobabo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'majibacoa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'manati']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'puerto padre']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'las tunas']);

        $province = $province->where('label', '=', 'holguin')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'antilla']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'baguanos']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'banes']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cacocum']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'calixto garcia']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cueto']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'frank pais']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'gibara']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'holguin']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'mayari']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'moa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'rafael freyre']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'sagua de tanamo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'urbano noris']);

        $province = $province->where('label', '=', 'granma')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'bartolome maso']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'bayamo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'buey arriba']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'campechuela']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'cauto cristo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guisa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'jiguani']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'manzanillo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'media luna']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'niquero']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'pilon']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'rio cauto']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'yara']);

        $province = $province->where('label', '=', 'santiago de cuba')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'contramaestre']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guama']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'mella']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'palma soriano']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san luis']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'santiago de cuba']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'segundo frente']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'songo-la maya']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'tercer frente']);

        $province = $province->where('label', '=', 'guantanamo')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'baracoa']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'caimanera']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'el salvador']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'guantanamo']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'imias']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'maisi']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'manuel tames']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'niceto perez']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'san antonio del sur']);
        $municipality->insert(['province_id' => $province->id, 'label' => 'yateras']);

        $province = $province->where('label', '=', 'isla de la juventud')->first();

        $municipality->insert(['province_id' => $province->id, 'label' => 'nueva gerona']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipality');
    }
}
