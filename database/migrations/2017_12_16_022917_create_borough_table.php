<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoroughTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borough', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('municipality_id')->nullable();
            $table->unsignedInteger('province_id');
            $table->string('code', 40);
            $table->string('label', 100);
            $table->timestamps();

            $table->foreign('municipality_id')->references('id')->on('municipality');
            $table->foreign('province_id')->references('id')->on('province');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borough');
    }
}
