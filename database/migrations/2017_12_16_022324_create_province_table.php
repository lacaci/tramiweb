<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Country;
use \App\Models\Province;


class CreateProvinceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('country_id');
            $table->string('label', 40);
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('country');
        });

        $country = new Country();
        $province = new Province();

        $country = $country->where('code', '=', 'CUB')->first();

        $province->insert(['country_id' => $country->id, 'label' => 'pinar del rio']);
        $province->insert(['country_id' => $country->id, 'label' => 'artemisa']);
        $province->insert(['country_id' => $country->id, 'label' => 'ciudad de la habana']);
        $province->insert(['country_id' => $country->id, 'label' => 'mayabeque']);
        $province->insert(['country_id' => $country->id, 'label' => 'matanzas']);
        $province->insert(['country_id' => $country->id, 'label' => 'cienfuegos']);
        $province->insert(['country_id' => $country->id, 'label' => 'villa clara']);
        $province->insert(['country_id' => $country->id, 'label' => 'sancti spiritus']);
        $province->insert(['country_id' => $country->id, 'label' => 'ciego de avila']);
        $province->insert(['country_id' => $country->id, 'label' => 'camaguey']);
        $province->insert(['country_id' => $country->id, 'label' => 'las tunas']);
        $province->insert(['country_id' => $country->id, 'label' => 'granma']);
        $province->insert(['country_id' => $country->id, 'label' => 'holguin']);
        $province->insert(['country_id' => $country->id, 'label' => 'santiago de cuba']);
        $province->insert(['country_id' => $country->id, 'label' => 'guantanamo']);
        $province->insert(['country_id' => $country->id, 'label' => 'isla de la juventud']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province');
    }
}
