<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/login', 'PagesController@login');

Route::get('/client/recipient/search', 'ClientRecipientController@search');
Route::get('/client/recipient/show', 'ClientRecipientController@show');
Route::get('/client/recipient/create', 'ClientRecipientController@create');

Route::get('/client/sender/search', 'ClientSenderController@search');
Route::get('/client/sender/show', 'ClientSenderController@show');
Route::get('/client/sender/create', 'ClientSenderController@create');

Route::get('/shipment/search', 'ShipmentController@search');
Route::get('/shipment/show', 'ShipmentController@show');
Route::get('/shipment/create', 'ShipmentController@create');

Route::get('/trip/search', 'TripController@search');
Route::get('/trip/show', 'TripController@show');
Route::get('/trip/create', 'TripController@create');
