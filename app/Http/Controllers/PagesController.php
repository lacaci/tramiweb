<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(Request $request)
    {
        return view('main');

    }

    public function login(Request $request)
    {
        return view('login');
    }
}
