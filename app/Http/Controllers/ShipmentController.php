<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShipmentController extends Controller
{
    public function search(Request $request)
    {
        return view('shipment.search');
    }

    public function show(Request $request)
    {
        return view('shipment.show');
    }

    public function create(Request $request)
    {
        return view('shipment.create');
    }

}
