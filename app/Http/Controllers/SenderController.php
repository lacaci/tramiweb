<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SenderController extends Controller
{
    public function search(Request $request)
    {
        return view('sender.search');
    }

    public function show(Request $request)
    {
        return view('sender.show');
    }

    public function create(Request $request)
    {
        return view('sender.create');
    }
}
