<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecipientController extends Controller
{
    public function search(Request $request)
    {
        return view('recipient.search');
    }

    public function show(Request $request)
    {
        return view('recipient.show');
    }

    public function create(Request $request)
    {
        return view('recipient.create');
    }
}
