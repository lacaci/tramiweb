<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TripController extends Controller
{

    public function search(Request $request)
    {
        return view('trip.search');
    }

    public function show(Request $request)
    {
        return view('trip.show');
    }

    public function create(Request $request)
    {
        return view('trip.create');
    }
}
